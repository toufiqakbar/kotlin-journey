package com.toufiq.viewmodelexample

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class ViewModelChallengeViewModelFactory(private val startingTotal: Double) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ViewModelChallengeViewModel::class.java)) {
            return ViewModelChallengeViewModel(startingTotal) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}