package com.toufiq.viewmodelexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.toufiq.viewmodelexample.databinding.ActivityViewModelChallengeBinding

class ViewModelChallengeActivity : AppCompatActivity() {
    private val TAG: String = "ViewModelChallengeTag"
    private lateinit var binding: ActivityViewModelChallengeBinding
    private lateinit var viewModel: ViewModelChallengeViewModel
    private lateinit var viewModelFactory: ViewModelChallengeViewModelFactory
    private var total: Double = 0.0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_model_challenge)
        viewModelFactory = ViewModelChallengeViewModelFactory(0.0)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(ViewModelChallengeViewModel::class.java)
        binding.apply {
            textViewNumber.text = viewModel.getTotal().toString()
            buttonAdd.setOnClickListener {
                if (editTextNumber.text.isNotEmpty()) {
                    viewModel.addTotal(editTextNumber.text.toString().toDouble())
                    textViewNumber.text = viewModel.getTotal().toString()
                }

            }

        }
        Log.d(TAG, "$total")

    }
}