package com.toufiq.viewmodelexample

import androidx.lifecycle.ViewModel

class ViewModelChallengeViewModel(startingTotal: Double) : ViewModel() {


    private var total: Double = 0.0

    init {
        total = startingTotal
    }

    fun addTotal(value: Double): Double {
        total += value
        return total
    }

    fun getTotal(): Double {
        return total
    }
}