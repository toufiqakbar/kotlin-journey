package com.toufiq.livedataexample.challenge

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ChallengeActivityViewModel() : ViewModel() {

    private var number = MutableLiveData<Int>(0)

    val countData: LiveData<Int>
        get() = number

    fun increment() {
        number.value = number.value?.plus(1)
    }
}