package com.toufiq.livedataexample

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel(startingTotal: Double) : ViewModel() {


    val totalData: LiveData<Double>
        get() = total


    private var total = MutableLiveData<Double>(0.0)

    init {
        total.value = startingTotal
    }

    fun addTotal(value: Double) {
        total.value = (total.value)?.plus(value)
    }

}