package com.toufiq.livedataexample.challenge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.toufiq.livedataexample.MainActivityViewModelFactory
import com.toufiq.livedataexample.R
import com.toufiq.livedataexample.databinding.ActivityChallengeBinding

class ChallengeActivity : AppCompatActivity() {
    lateinit var binding: ActivityChallengeBinding
    lateinit var viewModel: ChallengeActivityViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_challenge)
        viewModel = ViewModelProvider(this).get(ChallengeActivityViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
    }
}