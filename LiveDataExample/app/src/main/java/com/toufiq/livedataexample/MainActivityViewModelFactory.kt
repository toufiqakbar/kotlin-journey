package com.toufiq.livedataexample

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.toufiq.livedataexample.challenge.ChallengeActivityViewModel
import java.lang.IllegalArgumentException

class MainActivityViewModelFactory(private val startingTotal: Double) :
    ViewModelProvider.Factory {


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainActivityViewModel::class.java)) {
            return MainActivityViewModel(startingTotal) as T
        } else if (modelClass.isAssignableFrom(ChallengeActivityViewModel::class.java)) {
            return ChallengeActivityViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}