package com.toufiq.peoplecounterapp

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    lateinit var btn: Button
    lateinit var textView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn = findViewById(R.id.button2)
        textView = findViewById(R.id.textView)
        var i = 0
        btn.setOnClickListener { v ->
            textView.text = i.toString()
            i++
            Snackbar.make(v, "$i times clicked", Snackbar.LENGTH_LONG).show()
        }

    }

}