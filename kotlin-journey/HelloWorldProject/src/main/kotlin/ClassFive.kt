import models.BasicInfoProvider
import models.Person
import models.PersonInfoProvider
import models.checkTypes

fun main() {
    val provider =object : PersonInfoProvider{
        override val providerInfo: String
            get() = "anonymous"

    }
    provider.printInfo(Person("arthur","knight"))

}
