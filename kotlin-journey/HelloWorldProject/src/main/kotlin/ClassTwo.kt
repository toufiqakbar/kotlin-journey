fun getGreeting(greeting: String, name: String) = "$greeting, $name"


fun getGreetingUnit() = "Hello"


fun sayHello() = println(getGreetingUnit())


fun main() {
    println(getGreeting("Hey","Shawon"))
    sayHello()
}