import java.util.*

//class Entity private constructor(val id: String) {
//    companion object Factory : IdProvider {
//        val id: String = "id"
//        fun create() = Entity(getID())
//        override fun getID(): String {
//            return "this is your ID"
//        }
//    }
//}
//
//interface IdProvider {
//    fun getID(): String
//}

enum class EntityType {
    HELP, EASY, MEDIUM, HARD;

    fun getFormatted() = name.lowercase(Locale.getDefault())
        .replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
}

object EntityFactory {
    fun create(entityType: EntityType): Entity {

        val name = when (entityType) {
            EntityType.EASY -> entityType.getFormatted()
            EntityType.MEDIUM -> entityType.getFormatted()
            EntityType.HARD -> entityType.getFormatted()
            EntityType.HELP -> entityType.getFormatted()
        }

        return when (entityType) {
            EntityType.EASY -> Entity.Easy(UUID.randomUUID().toString(), name)
            EntityType.MEDIUM -> Entity.Medium(UUID.randomUUID().toString(), name)
            EntityType.HARD -> Entity.Hard(UUID.randomUUID().toString(), name, 2f)
            EntityType.HELP -> Entity.Help
        }
    }
}


sealed class Entity() {

    object Help : Entity() {
    }

    data class Easy(val id: String, val name: String) : Entity()
    data class Medium(val id: String, val name: String) : Entity()
    data class Hard(val id: String, val name: String, val mul: Float) : Entity()
}

fun Entity.Medium.printInfo() {
    println("Medium class : $id")
}

val Entity.Medium.info: String
    get() = "some Info"

fun main() {
    val entity: Entity = EntityFactory.create(EntityType.MEDIUM)

    val message = when (entity) {
        is Entity.Easy -> "Easy Class"
        is Entity.Hard -> "Hard Class"
        Entity.Help -> "Help Class"
        is Entity.Medium -> "Medium Class"
    }
    println(message)

    val e1 = Entity.Easy("id", "name")
    val e2 = e1.copy(name = "new Name")
    if (e1 == e2) {
        println("equal")
    } else {
        println("not equal")
    }

    if (entity is Entity.Medium) {
        entity.printInfo()
        entity.info
    }

}