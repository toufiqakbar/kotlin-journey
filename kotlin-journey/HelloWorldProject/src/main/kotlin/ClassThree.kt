fun main() {

    val interestingThings = mutableListOf("1", "2", "3", "Comic", "Books")
    interestingThings.map { interestingThing ->
        println(interestingThing)
    }

    interestingThings.forEachIndexed { index, interestingThing ->
        println("$interestingThing is at index $index")
    }

    val map = mapOf(1 to "a", "key" to "value")
    map.forEach { (key, value) ->
        println("$key and $value")
    }

    val mutMap = mutableMapOf(15 to "a", "X" to "value")
    mutMap["a"] = "9"

    mutMap.forEach { (key, value) ->
        println("$key and $value")
    }

}

