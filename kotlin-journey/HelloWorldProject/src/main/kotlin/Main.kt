fun main(args: Array<String>) {
    // var is mutable ,
    // val is immutable local variable


//    when (greeting) {//like switch
//        null -> println("hi")
//        else -> println(greeting)
//    }
//    greeting = "hello"
//    if (greeting != null) {
//        println(greeting)
//    } else {
//        println("hi")
//    }
//    val greetingToPrint = if (greeting != null) greeting else "hi"
//    val greetToPrint2 = when (greeting) {
//        null -> println("hi")
//        else -> println(greeting)
//    }
//
//    println(name)
    val names = arrayOf("1", "2", "3", "Comic", "Books")
    sendGreetings(names = names)
}

fun sendGreetings(greetings: String = "Hello", vararg names: String) {
    names.forEach { itemToGreet ->
        println("$greetings to $itemToGreet")
    }
}

const val name = "Shawon"
var greeting: String? = null