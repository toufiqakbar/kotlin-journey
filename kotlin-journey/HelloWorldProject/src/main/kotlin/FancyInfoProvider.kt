import models.BasicInfoProvider
import models.Person

class FancyInfoProvider : BasicInfoProvider() {
    override val providerInfo: String
        get() = "batman"

    override val sessionPrefix: String
        get() = "superman"
}
//
//fun main() {
//    val fancy = FancyInfoProvider()
//    fancy.printInfo(Person("Arthur", "Harrow"))
//    println(fancy.getSessionId())
//}