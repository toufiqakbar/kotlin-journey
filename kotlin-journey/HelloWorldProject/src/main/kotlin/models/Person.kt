package models

class Person(val firstName: String = "", val lastName: String = "") {
    //    init {
//        println("init 1")
//    }
//
//    constructor() : this("Peter", "Quil") {
//        println("Secondary Const")
//    }
//
//    init {
//        println("init 2")
//    }
//    val firstName: String=""
//    val lastName: String=""
//
//    init {
//        this.firstName = firstName
//        this.lastName = lastName
//    }
    var nickName: String? = null
        set(value) {
            field = value
//            println("value assigned as $value")
        }
        get() {
//            println("return value as $field")
            return field
        }

    fun printInfo() {
        println("${firstName}:${lastName}:${nickName?:"no nick"}")
    }
}