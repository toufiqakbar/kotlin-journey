package models

//interface PersonInfoProvider {
//    fun getHello(person: Person): String
//}
//
//class BasicInfoProvider : PersonInfoProvider {
//
//    override fun getHello(person: Person): String {
//        return "info is ${person.printInfo()}"
//    }
//
//
//}

interface PersonInfoProvider {
    val providerInfo: String
    fun printInfo(person: Person) {
        println(providerInfo)
        person.printInfo()
    }
}

open class BasicInfoProvider : PersonInfoProvider, SessionInfoProvider {
    override val providerInfo: String
        get() = "BasicInfoProvider"

    protected open val sessionPrefix:String = "Session"

    override fun printInfo(person: Person) {
        super.printInfo(person)
        println("new lines of code")
    }

    override fun getSessionId(): String {
        return sessionPrefix
    }

}

fun checkTypes(infoProvider: PersonInfoProvider) {
    if (infoProvider is SessionInfoProvider) {
        println("is Session Info Provider")
        infoProvider.getSessionId()
    } else {
        println("not Session info provider")
    }
}

interface SessionInfoProvider {
    fun getSessionId(): String
}