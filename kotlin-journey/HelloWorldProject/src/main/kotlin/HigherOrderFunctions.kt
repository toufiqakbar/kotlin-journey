import java.util.*

fun listFunctionForExample(list: List<String>, funct: ((String) -> Boolean)?) {

    list.forEach { name ->
        if (funct?.invoke(name) == true) {
            println(name)
        }
    }
}

val predicate: (String) -> Boolean =
    { name -> name.lowercase(Locale.getDefault()).startsWith("j") }

fun getPrintPredicate(): (String) -> Boolean {
    return predicate
}

fun main() {
    val list = mutableListOf("Java", "Kotlin", "C++", "JavaScript", null)

//    listFunctionForExample(list, getPrintPredicate())

//    list.filterNotNull().filter { it.startsWith("J") }.map { it.length }.forEach { println(it) }

//    list.filterNotNull().takeLast(2).forEach { println(it) }
    list.filterNotNull().associateWith { it.length }.forEach { println("${it.key} key ,${it.value} value") }
    println( list.filterNotNull().find { it.startsWith("x") }.orEmpty())
}
