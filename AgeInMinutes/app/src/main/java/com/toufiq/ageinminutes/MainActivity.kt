package com.toufiq.ageinminutes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import com.toufiq.ageinminutes.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.apply {
            button.setOnClickListener {
                textView.visibility = View.VISIBLE
                textView.text = "Hello " + textInputEditText.text
            }
            textInputEditText.addTextChangedListener { editable ->
                textView.visibility = View.VISIBLE
                textView.text = editable.toString()
            }
        }
    }

}