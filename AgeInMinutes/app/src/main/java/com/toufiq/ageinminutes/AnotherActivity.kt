package com.toufiq.ageinminutes

import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.toufiq.ageinminutes.databinding.ActivityAnotherBinding

class AnotherActivity : AppCompatActivity() {
    lateinit var binding: ActivityAnotherBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_another)
        binding.apply {

            button.setOnClickListener {
                if (progressBar.visibility == View.INVISIBLE) {
                    button.text = "Stop"
                    progressBar.visibility = View.VISIBLE
                } else {
                    button.text = "Start"
                    progressBar.visibility = View.INVISIBLE
                }
            }

        }
    }
}