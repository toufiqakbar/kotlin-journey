package com.toufiq.ageinminutes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.toufiq.ageinminutes.databinding.ActivityLayoutBindingBinding
import com.toufiq.ageinminutes.models.Student

class LayoutBinding : AppCompatActivity() {
    lateinit var binding: ActivityLayoutBindingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_layout_binding)
        binding.student=getStudent()
    }

    private fun getStudent(): Student {
        return Student(1, "Shawon", "s@a.com")
    }
}