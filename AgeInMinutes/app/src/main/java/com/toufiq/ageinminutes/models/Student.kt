package com.toufiq.ageinminutes.models

data class Student(
    var id: Int,
    var name: String,
    var email: String
)