package com.toufiq.twowaydatabinding

import androidx.databinding.Bindable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel : ViewModel() {

//    val getName: LiveData<String>
//        get() = uName

    var uName = MutableLiveData<String>()


    init {
        uName.value = "Frank"
    }
}